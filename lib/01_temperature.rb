# 1 degree F = 5/9 degree Celsius
# 1 degree C = 9/5 degree Fahrenheit


def ftoc(ftemp)
  c = (ftemp - 32) * (5.0 / 9.0)
  c.round(1)
end

def ctof(ctemp)
  f = ctemp * (9.0 / 5.0) + 32
  f.round(1)
end
