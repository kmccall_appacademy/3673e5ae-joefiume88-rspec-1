def add(a, b)
  res = a + b
end

def subtract(a, b)
  res = a - b
end

def sum(arr)
  res = arr.reduce(:+) || 0
end

def multiply(arr)
  res = arr.reduce(:*)
end

def power(a, b)
  res = a ** b
end

def factorial(num)
  return 1 if num == 0
  factorial_array = (1..num).to_a
  res = factorial_array.reduce(:*)
end
