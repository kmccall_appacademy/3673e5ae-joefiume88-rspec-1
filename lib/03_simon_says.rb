def echo(word)
  word
end

def shout(phrase)
  phrase.upcase
end

def repeat(word, num=2)
  res = ''
  num.times do
    res << word + ' '
  end

  res[0...-1]
end

def start_of_word(word, num)
  word[0..(num-1)]
end

def first_word(phrase)
  phrase_arr = phrase.split(' ')
  phrase_arr.first
end

def titleize(title)
  res = ''
  little_words = ['and', 'the', 'of', 'over']

  title.split(' ').each_with_index do |word, idx|
    if little_words.include?(word) && idx != 0
      res << word + ' '
    else
      res << word.capitalize + ' '
    end
  end
  
  res[0...-1]
end
