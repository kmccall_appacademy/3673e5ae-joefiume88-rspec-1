def translate(string)
  res = ''
  
  string.split(' ').each do |word|
    res << latinize(word) + ' '
  end

  res[0...-1]
end



def latinize(word)
  vowels = 'aeiou'

  if vowels.include?(word[0])
    word + 'ay'

  else
    until vowels.include?(word[0]) && (word[-1] + word[0] != 'qu')
      word = word[1..-1] + word[0]
    end

    word + 'ay'
  end
end
